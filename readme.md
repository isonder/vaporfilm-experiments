# Vapor Film Experiments

<img src="data/img/example_blank-and-marked.png"
     style="width: 50%;"
     alt="Example of film thickness measurement markup"/>
<img src="data/img/example_area-markup.png"
     style="width: 50%;"
     alt="Example of direct contact area measurement markup"/>

Experimental analysis can be viewed either directly, or on NBViewer.
- **Run 1:**
  Water pool temperature was 65°C.
  - *Film thickness:*
    [`run1/thickness.ipynb`](https://nbviewer.org/urls/gitlab.com/isonder/vaporfilm-experiments/-/raw/master/run1/thickness.ipynb/?inline=false)
  - *Direct contact area:*
    [`run1/area.ipynb`](https://nbviewer.org/urls/gitlab.com/isonder/vaporfilm-experiments/-/raw/master/run1/area.ipynb/?inline=false)
- **Run 2:**
  Water pool temperature was 3.0°C.
  - *Direct contact area:*
    [`run2/area.ipynb`](https://nbviewer.org/urls/gitlab.com/isonder/vaporfilm-experiments/-/raw/master/run2/area.ipynb/?inline=false)
- **Run 3:**
  Water pool temperature was 93°C.
  - *Direct contact area*
    [`run3/area.ipynb`](https://nbviewer.org/urls/gitlab.com/isonder/vaporfilm-experiments/-/raw/master/run2/area.ipynb/?inline=false)

## Dataset and Pulication

Experiments and analysis are described in an article
[in Frontiers in Earth Science. (Doi: `10.3389/feart.2022.983112`)](https://doi.org/10.3389/feart.2022.983112)

All data lives as a data set
[on Zenodo (Doi: `10.5281/zenodo.6950485`)](https://doi.org/10.5281/zenodo.6950485).

## Run and Use the Code

The code in the notebooks is Python. To run them, three small of my packages
have to be installed first:
[`coordspicker`](https://gitlab.com/isonder/coordspicker),
[`twodpg`](https://gitlab.com/isonder/twodpg) and
[`vfetools`](https://gitlab.com/isonder/vfetools).

It is best to run and install these in a virtual environment.  
In a virtual environment clone the package repositories and locally install;
for example:
```sh
mkdir dependencies
cd dependencies
git clone https://gitlab.com/isonder/coordspicker.git
cd coordspicker
pip install -e .
# If you also want the Fiji plugins to work, adjust the plugins path below
# uncomment and run, or copy those files manually.
#cp roimanager_*.* path/to/Fiji.app/plugins
cd ..
git clone https://gitlab.com/isonder/twodpg.git
cd twodpg
pip install -e .
cd ..
git clone https://gitlab.com/isonder/vfetools.git
cd vfetools
pip install -e .
cd ../..
```
When done, clone this repo and install packages listed in the requirements file:
```sh
git clone https://gitlab.com/isonder/vaporfilm-experiments.git
cd vapor-film-experiments
pip install -r requirements.txt
```